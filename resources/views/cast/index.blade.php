@extends('layout.master')

@section('judul')
Data Pemain Film
@endsection

@section('content')
<a href="{{ route('cast.create') }}" class="btn btn-primary mb-3">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $actor)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $actor->nama }}</td>
                    <td>{{ $actor->umur }}</td>
                    <td>{{ $actor->bio }}</td>
                    <td>
                     <div class="btn-group">
                        <a href="/" class="btn btn-info">Show</a>
                        <a href="/" class="btn btn-primary">Edit</a>
                        <form action="/" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                     </div>
                    </td>
                </tr>
            @empty
                <tr colspan="5">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection